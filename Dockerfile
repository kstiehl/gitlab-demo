FROM openjdk:13.0.2-jdk-oracle

EXPOSE 8080
COPY build/libs/app.jar /app.jar
CMD java -jar /app.jar
